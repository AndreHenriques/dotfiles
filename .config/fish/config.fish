alias config='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias da='dragon-drag-and-drop --and-exit'
alias dat='dragon-drag-and-drop -t --and-exit'
alias dp='doas pacman'
alias p='pacman'
alias vim='nvim'
alias em="emacs -nw":
alias ls="exa"
alias ll="exa -al"
alias la="exa -a"
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias df="df -h"
alias du="du -h"
alias ledger="ledger -f $HOME/Documents/ledger/ledger.ledger"

set EDITOR "emacs"
set _JAVA_AWT_WM_NONREPARENTING "1"
set JAVA_HOME /usr/lib/jvm/java-8-openjdk
#export PATH=$PATH:$ANDROID_HOME/tools
#export PATH=$PATH:$ANDROID_HOME/tools/bin
#export PATH=$PATH:$ANDROID_HOME/platform-tools