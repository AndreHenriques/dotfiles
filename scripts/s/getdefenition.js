#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

var stdin = process.openStdin();

var data = "";

stdin.on('data', function(chunk) {
  data += chunk;
});

stdin.on('end', function() {
    data = String(data).replace('\n', '');
    if (data === '') process.exit(1);
    let result = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'dic.json')))[data];
    if (!result) process.exit(1);
    console.log(result.replace(/\. \(/g, '\n('));
    //console.log(result);
    process.exit(0);
});
